﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntersysRecruitmentProject.RecuruitmentTasks
{
    public class Palindrome
    {
        /// <summary>
        /// From given string it's getting only letters and checking if it's a palindrome. 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string IsStringPalindrome(string str)
        {
            if (string.IsNullOrEmpty(str)) return "YES";
            var onlyLettersString = new string(str.Where(char.IsLetter).ToArray());
            int oppositeIndex = -1;
            for (int i = 0; i < onlyLettersString.Length; i++)
            {
                oppositeIndex = (onlyLettersString.Length - 1) - i;
                if (i >= oppositeIndex) break;
                if (onlyLettersString[i] != onlyLettersString[oppositeIndex])
                {
                    return "NO";
                }
            }
            return "YES";
        }
    }
}
