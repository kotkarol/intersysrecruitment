﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntersysRecruitmentProject.Tools;

namespace IntersysRecruitmentProject.RecuruitmentTasks
{
    public class FindPrimes
    {
        public static string NumberOfPrimesInInterval(int leftBound, int? rightBound = null)
        {
            if (leftBound < 0 || !rightBound.HasValue) return "";
            if (rightBound > 10.GetPowerValue(9)) throw new ArgumentException("Right bound is to big!");
            if ((rightBound - leftBound) > 10.GetPowerValue(5))
            {
                throw new ArgumentException("Requested range is to big! Max range is 10^5!");
            }
            if (leftBound > rightBound) throw new ArgumentException("Left bound cannot be bigger than right one!");

            int counter = 0;
            int rightBoundValue = rightBound ?? leftBound;

            for (int i = leftBound; i <= rightBoundValue; i++)
            {
                if (i.IsPrimeNumber()) counter++;
            }
            return counter.ToString();
        }
    }
}
