﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntersysRecruitmentProject.Tools;

namespace IntersysRecruitmentProject.RecuruitmentTasks
{
    public class Powers
    {
        public static string GetPowersOfTwoInGivenList(IEnumerable<int> enumerable)
        {
            var powerList = new List<int>();
            foreach (var number in enumerable)
            {
                if (number.IsPowerOf(2)) powerList.Add(number);
            }
            if (!powerList.Any()) return "NA";
            StringBuilder sb = new StringBuilder();
            powerList.ForEach(x => sb.Append($"{x}, "));

            var returnString = sb.ToString();
            return returnString.Substring(0, returnString.Length - 2);
        }
    }
}
