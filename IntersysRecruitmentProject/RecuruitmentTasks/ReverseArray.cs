﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntersysRecruitmentProject.RecuruitmentTasks
{
    public class ReverseArray
    {
        /// <summary>
        /// Method is taking IEnumerable of any type an return it in reversed order.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arrayToReverse"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetReversedArray<T>(IEnumerable<T> arrayToReverse)
        {
            if (arrayToReverse == null || !arrayToReverse.Any() || arrayToReverse.Count() == 1) return arrayToReverse;
            T[] arrayToRetun = new T[arrayToReverse.Count()];
            for (int i = arrayToRetun.Count() - 1; i >= 0; i--)
            {
                arrayToRetun[(arrayToRetun.Length - 1) - i] = arrayToReverse.ElementAt(i);
            }
            return arrayToRetun;
        }
        /// <summary>
        /// Method reverse array given in parameter using linq method "Reverse".
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arrayToReverse"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetReversedArrayUsingLinq<T>(IEnumerable<T> arrayToReverse)
        {
            return arrayToReverse.Reverse();
        }
    }
}
