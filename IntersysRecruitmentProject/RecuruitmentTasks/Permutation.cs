﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntersysRecruitmentProject.RecuruitmentTasks
{
    public class Permutation
    {
        public static string IsTherePermutation(IEnumerable<int> arrayOne, IEnumerable<int> arrayTwo)
        {
            if (arrayOne.Count() != 11 || arrayTwo.Count() != 11) return "NO";

            var orderedOne = arrayOne.OrderBy(x => x);
            var orderedTwo = arrayTwo.OrderBy(x => x);
            for (int i = 0; i < orderedOne.Count(); i++)
            {
                if (orderedOne.ElementAt(i) != orderedTwo.ElementAt(i)) return "NO";
            }
            return "YES";
        }
    }
}
