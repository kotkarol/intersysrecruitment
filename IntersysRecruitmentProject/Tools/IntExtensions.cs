﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntersysRecruitmentProject.Tools
{
    public static class IntExtensions
    {
        /// <summary>
        /// Determinants if number is power of number given in argument.
        /// </summary>
        /// <param name="number"></param>
        /// <param name="requestedPower"></param>
        /// <returns></returns>
        public static bool IsPowerOf(this int number, int requestedPower)
        {
            if (requestedPower == 0 && number == 0) return true;
            if (requestedPower == 0) return false;
            int changedInt = number;
            while (changedInt % requestedPower == 0)
            {
                changedInt = changedInt / requestedPower;
                if (changedInt == 1) return true;
            }
            return false;
        }
        /// <summary>
        /// Returns value of "lookedpower" power of number. 
        /// </summary>
        /// <param name="number"></param>
        /// <param name="lookedpower"></param>
        /// <returns></returns>
        public static int GetPowerValue(this int number, int lookedpower)
        {
            return (int)Math.Pow(number, lookedpower);
        }
        /// <summary>
        ///  Determinants if number is prime number; 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static bool IsPrimeNumber(this int number)
        {
            if (number <= 3) return number > 1;
            if (number % 2 == 0 || number % 3 == 0) return false;
            var checkedValue = 5;
            while (checkedValue * checkedValue <= number)
            {
                if (number % checkedValue == 0 || number % (checkedValue + 2) == 0) return false;
                checkedValue += 1;
            }
            return true;
        }
        
    }
}
