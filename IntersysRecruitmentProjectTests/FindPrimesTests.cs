﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntersysRecruitmentProject.RecuruitmentTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IntersysRecruitmentProjectTests
{
    [TestClass]
    public class FindPrimesTests
    {
        [TestMethod]
        public void FindPrimes_NumberOfPrimesInInterval_Case1()
        {
            //1.Arrange
            //2.Act
            var resultOne = FindPrimes.NumberOfPrimesInInterval(1);
            var resultTwo = FindPrimes.NumberOfPrimesInInterval(1, 10);
            //3.Assert
            Assert.AreEqual("",resultOne);
            Assert.AreEqual("4", resultTwo);
        }
        [TestMethod]
        public void FindPrimes_NumberOfPrimesInInterval_Case2()
        {
            //1.Arrange
            //2.Act
            var resultOne = FindPrimes.NumberOfPrimesInInterval(2);
            var resultTwo = FindPrimes.NumberOfPrimesInInterval(100, 200);
            var resultThree = FindPrimes.NumberOfPrimesInInterval(1, 1);
            //3.Assert
            Assert.AreEqual("", resultOne);
            Assert.AreEqual("21", resultTwo);
            Assert.AreEqual("0", resultThree);
        }
    }
}
