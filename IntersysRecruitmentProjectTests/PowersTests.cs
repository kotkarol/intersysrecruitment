﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntersysRecruitmentProject.RecuruitmentTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IntersysRecruitmentProjectTests
{
    [TestClass]
    public class PowersTests
    {
        [TestMethod]
        public void Powers_GetPowersOfGivenNumberInList_Good()
        {
            //1.Arrange
            var list = new List<int> {2, 5, 16, 10, 64, 15, 1024, 67108864 , -2};
            //2.Act
            var str = Powers.GetPowersOfTwoInGivenList(list);
            //3.Assert
            Assert.IsTrue(!string.IsNullOrEmpty(str));
            Assert.IsTrue(str.Contains("1024"));
            Assert.IsTrue(str.Contains("2"));
            Assert.IsTrue(str.Contains("16"));
            Assert.IsTrue(str.Contains("64"));
            Assert.IsTrue(!str.Contains("15"));
            Assert.IsTrue(str.Contains("67108864"));
            Assert.IsTrue(!str.Contains("-2"));
        }
    }
}
