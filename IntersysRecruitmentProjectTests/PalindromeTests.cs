﻿using IntersysRecruitmentProject.RecuruitmentTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntersysRecruitmentProjectTests
{
    [TestClass]
    public class PalindromeTests
    {
        private string testWordGood = "a1-b\\c2@c$b2a";
        private string testWordAnotherGood = "a1-b\\c2@$b2a";
        private string testWordBad = "a1-b\\c2@$xb2a";

        [TestMethod]
        public void IsStringPalindrome_StringEmpty()
        {
            //1.Arrange 
            string emptyString = "";
            //2.Act
            var result =  Palindrome.IsStringPalindrome(emptyString);
            //3.Assert
            Assert.AreEqual("YES", result);
        }
        [TestMethod]
        public void IsStringPalindrome_StringNull()
        {
            //1.Arrange 
            string emptyString = null;
            //2.Act
            var result = Palindrome.IsStringPalindrome(emptyString);
            //3.Assert
            Assert.AreEqual("YES", result);
        }
        [TestMethod]
        public void IsStringPalindrome_Good()
        {
            //1.Arrange 
            string emptyString = null;
            //2.Act
            var result = Palindrome.IsStringPalindrome(testWordGood);
            //3.Assert
            Assert.AreEqual("YES", result);
        }
        [TestMethod]
        public void IsStringPalindrome_AnotherGood()
        {
            //1.Arrange 
            string emptyString = null;
            //2.Act
            var result = Palindrome.IsStringPalindrome(testWordAnotherGood);
            //3.Assert
            Assert.AreEqual("YES", result);
        }
        [TestMethod]
        public void IsStringPalindrome_Bad()
        {
            //1.Arrange 
            string emptyString = null;
            //2.Act
            var result = Palindrome.IsStringPalindrome(testWordBad);
            //3.Assert
            Assert.AreEqual("NO", result);
        }
    }

}
