﻿using IntersysRecruitmentProject.RecuruitmentTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntersysRecruitmentProjectTests
{
    [TestClass]
    public class ReverseArrayTests
    {
        private List<int> testList = new List<int>() { 1, 4, 5, 21, 51, 2, 22 };

        [TestMethod]
        public void GetReversedArray_EmptyArray()
        {
            //1. Arrange
            var list = new List<int> { };
            //2. Act
            var result = ReverseArray.GetReversedArray(list);
            //3. Assert
            Assert.AreEqual(result.FirstOrDefault(), result.LastOrDefault());
        }
        [TestMethod]
        public void GetReversedArray_NullArray()
        {
            //1. Arrange
            List<int> list = null;
            //2. Act
            var result = ReverseArray.GetReversedArray(list);
            //3. Assert
            Assert.AreEqual(result, list);
        }
        [TestMethod]
        public void GetReversedArray_SingleElement()
        {
            //1. Arrange
            List<int> list = new List<int> { 1 };
            //2. Act
            var result = ReverseArray.GetReversedArray(list);
            //3. Assert
            Assert.AreEqual(result.FirstOrDefault(), result.LastOrDefault());
        }
        [TestMethod]
        public void GetReversedArray_Good()
        {
            //1. Arrange
            //2. Act
            var result = ReverseArray.GetReversedArray(testList);
            //3. Assert
            Assert.AreEqual(result.FirstOrDefault(), testList.LastOrDefault());
            Assert.AreEqual(result.ElementAt(1), testList.ElementAt(testList.Count - 2 ));
            Assert.AreEqual(result.ElementAt(2), testList.ElementAt(testList.Count - 3 ));
        }
        [TestMethod]
        public void GetReversedArrayUsingLinq_Good()
        {
            //1. Arrange
            //2. Act
            var result = ReverseArray.GetReversedArrayUsingLinq(testList);
            //3. Assert
            Assert.AreEqual(result.FirstOrDefault(), testList.LastOrDefault());
            Assert.AreEqual(result.ElementAt(1), testList.ElementAt(testList.Count - 2));
            Assert.AreEqual(result.ElementAt(2), testList.ElementAt(testList.Count - 3));
        }
    }
}
